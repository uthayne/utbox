import os
import yaml

from UTBox.exceptions import ConfigurationError, PathError


def get_asmp_env():
    asmp_env = os.environ.get('ASMP_ENV')
    if asmp_env is None:
        raise ConfigurationError('Unable to determine ASMP_ENV.')

    return asmp_env


def get_config(path):
    try:
        with open(path, 'r') as config_file:
            config = yaml.load(config_file)
    except FileNotFoundError:
        return {}
    else:
        if config is None:
            return {}
        else:
            return config


def relpath(basepath, pathspec, basedir=False):
    """
    Useful for locating a path relative to a known resource (such as
    a python file).

    parameters:
        basepath: An base path (typically __file__).
        pathspec: A relative path; cannot begin with os.sep.
        basedir: If true, take basepath to be a directory
            (otherwise test it to decide if it is a directory or a file
            inside an _implied_ directory).
    """
    if pathspec.startswith(os.sep):
        raise PathError("The pathspec may not begin with the path "
                        "separator."
                        )
    if basedir or os.path.isdir(basepath):
        bdir = basepath
    else:
        bdir = os.path.dirname(basepath)

    if pathspec:  # avoid adding trailing slash when pathspec == ''
        path = os.path.join(bdir, pathspec)
    else:
        path = bdir

    path = os.path.normpath(path)
    return path

PROJ_CONFIG = relpath(__file__, "../project_config.yaml")

ASMP_ENV = get_asmp_env()
