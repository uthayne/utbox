import boxsdk
from boxsdk.exception import BoxAPIException
import json
import logging

from UTBox.config import get_config

from UTBox.exceptions import FolderError


JOB = 'delivery'
LOG = logging.getLogger('UTBox')


def get_box_client():
    """
    Returns an instance of boxsdk.Client().
    The default implementation just returns a boxsdk.DevelopmentClient
    which will prompt stdin for a user generated development key.
    A production implementation should use a more rebust client that
    takes advantage of the OAuth2 or JWT authentication options.
    """
    return boxsdk.DevelopmentClient()


def get_box_client_oauth():
    doc = get_config('project_config.yaml')
    access_token, refresh_token = read_tokens(doc['TOKEN_FILENAME'])

    oauth = boxsdk.OAuth2(
        client_id=doc['CLIENT_ID'],
        client_secret=doc['CLIENT_SECRET'],
        access_token=access_token,
        refresh_token=refresh_token,
        store_tokens=store_tokens
    )

    client = boxsdk.Client(oauth)

    return client


def store_tokens(access_token, refresh_token, filename=None):

    LOG.info('Storing new token')

    if filename is None:
        doc = get_config('project_config.yaml')
        filename = doc['TOKEN_FILENAME']

    with open(filename, "r+") as f:
        token = json.load(f)
        token["access"] = access_token
        token["refresh"] = refresh_token
        f.seek(0)
        json.dump(token, f)
        f.truncate()


def read_tokens(filename):
    LOG.info('reading tokens')
    with open(filename) as token_file:
        token = json.load(token_file)

    return token["access"], token["refresh"]


def get_folder(parent, folder_name):
    """
    Returns an instance of boxsdk.object.folder.Folder().
    This searches the subfolders of the parent folder for
    a folder with the specified name.
    """
    # Get subitems from parent
    sub_items = parent.get_items(limit=1000)

    # Search for the name in the subitems
    for item in sub_items:
        if type(item) is boxsdk.object.folder.Folder:
            if item['name'] == folder_name:
                return item

    LOG.error('Folder not found.')
    raise FolderError('Folder {0} does not exist and folder '
                      'creation was not allowed.'.format(folder_name))


def get_reports_folder(client, folder):
    """
    Returns an instance of boxsdk.object.folder.Folder().
    """
    # Constant: Tuple(n)
    # Represents the ordered route to the desired folder for reports.
    # Note: ints and strings have a different meaning here. ints are
    # box ids for folders, whereas strings are names of folders.
    # The 0 id folder represents the root level and MUST be first.
    # Default: (0, 'Box Reports')
    BOX_REPORT_FOLDER = (0, folder)

    # The first folder must be the root folder
    assert BOX_REPORT_FOLDER[0] == 0

    cur_folder = None

    # Iterate through the remaining folders
    for folder in BOX_REPORT_FOLDER:
        # Folder is a Box ID
        if type(folder) is int:
            cur_folder = client.folder(folder_id=folder)
        # Folder is a name
        elif type(folder) is str:
            cur_folder = get_folder(cur_folder, folder)
        else:
            raise TypeError("Folders must be either an int or str.")

    return cur_folder


def upload_report(filepath, folder):
    """
    Uploads a report to Box in the specified folder.
    report should be an OS file URI pointing to the report
    that will be updated. Ex: ~/Reports/01-01-1970-report.xlxs
    """
    # Create the box Client sdk wrapper
    client = get_box_client_oauth()

    # Get the folder being used for reports
    reports_folder = get_reports_folder(client, folder)

    # Upload the new report to box
    try:
        box_report = reports_folder.upload(file_path=filepath)
    except BoxAPIException as e:
        LOG.error('Error uploading file: {0}'.format(e))
        raise

    # Verify upload worked
    assert box_report['type'] == 'file'
    assert box_report['id'] is not None and int(box_report['id']) > 0

    # Output results
    LOG.info('Successfully uploaded report {0} to Box in '
             'folder {1}.'.format(box_report['name'], folder))

    shared_link = client.folder(folder_id=reports_folder['id']).get_shared_link()
    file = client.file(file_id=box_report['id']).get()['name']

    LOG.info('File {0} successfully uploaded to {1}.'.format(file, shared_link))
