import argparse
import sys
import logging

from UTBox.deliver import upload_report
from UTBox.utils.log_support import setup_logging


LOG = logging.getLogger('UTBox')
JOB = 'Job=deliver'

class Main(object):
    # def __init__(self):

    def get_args(self):
        return sys.argv[1:]

    def get_parser(self):
        parser = argparse.ArgumentParser('Tranfer file onto UTBox.')
        parser.add_argument('filepath',
                            type=str,
                            help='The file to transfer to box.'
                            )
        parser.add_argument('folder',
                            type=str,
                            help='The folder to move the file to on UT Box.'
                            )

        return parser

    def parse_run_options(self):
        args = self.get_args()
        parser = self.get_parser()
        ns = parser.parse_args(args)
        return ns

    def run(self):
        setup_logging()

        ns = self.parse_run_options()

        upload_report(ns.filepath, ns.folder)
